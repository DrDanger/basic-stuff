import {Component, Input} from '@angular/core' ;
import {FormsModule} from '@angular/forms'


@Component({

selector: 'app-products',
templateUrl: './products.component.html',

})

export class ProductsComponent {
// tslint:disable-next-line: no-inferrable-types
productName: 'A book';
isDisabled = true;
products = ['A book', 'A tree'];

constructor() {
   setTimeout(() => {
  //    this.productName = 'A tree';
this.isDisabled = false;
}, 3000);
 }
 onAddProduct(form) {
  if (form.valid){
    this.products.push(form.value.productName);
  }
 }

onRemoveProduct(productName: string){
  this.products = this.products.filter(p => p !== productName);
// last 1 or 2 videos short
}
}

